import { Component, OnInit } from '@angular/core';
import { Articulo } from 'src/app/modelos/articulo';
import { ArticulosService } from 'src/app/servicios/articulos.service';

@Component({
  selector: 'app-articulo-table',
  templateUrl: './articulo-table.component.html',
  styleUrls: ['./articulo-table.component.scss']
})
export class ArticuloTableComponent implements OnInit {
 articuloslista:Articulo[]=[];
  constructor(private articuloS:ArticulosService) {
    this.articuloslista = articuloS.getArticulos();
   }
   print(id:string){
     console.log(id)
   }

  ngOnInit(): void {
  }

}
