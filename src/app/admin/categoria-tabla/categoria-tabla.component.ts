import { Router } from '@angular/router';
import { CategoriasService } from './../../servicios/categorias.service';
import { Categorias } from 'src/app/modelos/categorias';
import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-categoria-tabla',
  templateUrl: './categoria-tabla.component.html',
  styleUrls: ['./categoria-tabla.component.scss']
})
export class CategoriaTablaComponent implements OnInit {

  categorialista: Categorias[] = []

  constructor(private categoriaS: CategoriasService, private ruta: Router) {
    this.categoriaS.verCategoria().subscribe(val => {
      this.categorialista = val
      console.log(this.categorialista)
    });
    console.log()
  }

  imprimir(id: string) {
    console.log(id)
  }
  ngOnInit(): void {
  }
eliminar(id:string){
  this.categoriaS.borrarCategoria(id)
}
modificar(id: any){
  this.ruta.navigate(['/admin/fCategoria/'+id])
  return id;
}
}
