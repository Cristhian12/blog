import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { CompartidoModule } from '../compartido/compartido.module';
import { InicioAdminComponent } from './inicio-admin.component';
import { FormArticuloComponent } from './form-articulo/form-articulo.component';
import { FormCategoriaComponent } from './form-categoria/form-categoria.component';
import { ArticuloTableComponent } from './articulo-table/articulo-table.component';
import { CategoriaTablaComponent } from './categoria-tabla/categoria-tabla.component'

@NgModule({
  declarations: [
    InicioAdminComponent,
    FormArticuloComponent,
    FormCategoriaComponent,
    ArticuloTableComponent,
    CategoriaTablaComponent
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    CompartidoModule
  ]
})
export class AdminModule { }
