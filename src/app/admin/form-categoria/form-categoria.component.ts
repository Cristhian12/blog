import { CategoriasService } from './../../servicios/categorias.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Categorias } from 'src/app/modelos/categorias';

@Component({
  selector: 'app-form-categoria',
  templateUrl: './form-categoria.component.html',
  styleUrls: ['./form-categoria.component.scss']
})
export class FormCategoriaComponent implements OnInit {

  categoriaNueva: Categorias = {
    nombre: "",
    color: "",
  }
  route: string = "";
  constructor(private rutaS: ActivatedRoute, private Categorias: CategoriasService, private rutas: Router) {
    const dir = this.rutaS.snapshot.params.rueda;

    if (dir === 'Crear') {
      this.categoriaNueva = {
        nombre: '',
        color: '',
      }
    } else {
      this.Categorias.buscarCategoria(this.rutaS.snapshot.params.route).subscribe(val => {
        this.categoriaNueva = val;
      })

    }
  }

  ngOnInit(): void {
  }

  crearCategoria(categoria: Categorias) {
    if (categoria.id) {
      this.Categorias.editarSoftCategoria(categoria).then(() => {
        this.rutas.navigateByUrl("/admin/TableC")
      }
      );;

    } else {
      this.Categorias.crearCategoria(categoria).then(() => {
        this.rutas.navigateByUrl("/admin/TableC")
      }
      );
    }
  }
}