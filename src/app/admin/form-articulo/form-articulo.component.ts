import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Articulo } from 'src/app/modelos/articulo';
import { ArticulosService } from 'src/app/servicios/articulos.service';

@Component({
  selector: 'app-form-articulo',
  templateUrl: './form-articulo.component.html',
  styleUrls: ['./form-articulo.component.scss']
})
export class FormArticuloComponent implements OnInit {

  avionForm: Articulo = {
    id: "",
    titulo: "",
    descripcion: "",
    imagen: "",
    autor: "",
    fechaCreacion: "",
    categorias: []

  };
  article: any;
  routes: string = "";

  constructor(private rutaS: ActivatedRoute, private avion: ArticulosService) {
    this.rutaS.params.subscribe(x => {
      this.routes = x.route;
      if (this.routes !== "crear"){
        this.avionForm = this.avion.getArticuloId(this.routes);
      }
  });
}

ngOnInit(): void {
}

sentir() {

  console.log(this.avionForm)
}
}