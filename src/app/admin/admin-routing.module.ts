import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ArticuloTableComponent } from './articulo-table/articulo-table.component';
import { CategoriaTablaComponent } from './categoria-tabla/categoria-tabla.component';
import { FormArticuloComponent } from './form-articulo/form-articulo.component';
import { FormCategoriaComponent } from './form-categoria/form-categoria.component';
import { InicioAdminComponent } from './inicio-admin.component';

const routes: Routes = [
  {
    path: "",
    component: InicioAdminComponent,
    children: [
      {
        path: "fArticulo/:route",
        component: FormArticuloComponent,
      },
      {
        path: "fCategoria/:route",
        component: FormCategoriaComponent,
      },
      {
        path: "fTable",
        component: ArticuloTableComponent,
      },
      {
        path: "TableC",
        component: CategoriaTablaComponent,
      },

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
