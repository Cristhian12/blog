import { Component, OnInit } from '@angular/core';
import { faMapLocation } from '@fortawesome/free-solid-svg-icons';
import {faPhone} from  '@fortawesome/free-solid-svg-icons';
import { faEnvelope } from '@fortawesome/free-solid-svg-icons';
import { Contacto } from '../modelos/contacto';
@Component({
  selector: 'app-contacto',
  templateUrl: './contacto.component.html',
  styleUrls: ['./contacto.component.scss']
})
export class ContactoComponent implements OnInit {
  myIcon=faMapLocation;
  myIcon2=faPhone;
  myIcon3= faEnvelope;
  miContacto: Contacto={

    id:'',
    correo:'',
    telefono:'',
    mensaje:'',
    nombre:'',
  };

  constructor() { }


  ngOnInit(): void {
  }
  sentir(){
    console.log(this.miContacto) 
  }
}
