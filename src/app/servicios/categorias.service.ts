import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/compat/firestore';
import { Categorias } from '../modelos/categorias';
import {map} from'rxjs/operators'
import { merge, Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class CategoriasService {

  direccion:AngularFirestoreCollection<Categorias>
  constructor(private db: AngularFirestore) { 
    this.direccion = this.db.collection<Categorias>("categorias")
  }

  verCategoria(): Observable<Categorias[]>{
    return this.direccion.snapshotChanges().pipe(
      map(base => base.map(separados =>{
        const id =  separados!.payload.doc.id
        const docu = separados.payload.doc.data() as Categorias
        return {id, ...docu}
      }))
    )
  }

 buscarCategoria(id:string){
 return this.direccion.doc(id).snapshotChanges().pipe(
   map(separados =>{
     const id= separados.payload.id
     const docu = separados.payload.data() as Categorias
     return {id, ...docu}
   })
 )
 }
 crearCategoria(cat:Categorias){
   return this.direccion.add(cat)
 }
 editarHardCategoria(cat:Categorias){
 return this.direccion.doc(cat.id).update(cat)
 }
 editarSoftCategoria(cat:Categorias){
   return this.direccion.doc(cat.id).set(cat, {merge: true})

 }
 borrarCategoria(id:string){
  return this.direccion.doc(id).delete()
 }
}