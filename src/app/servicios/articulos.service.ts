import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/compat/firestore';
import { Injectable } from '@angular/core';

import { Articulo } from '../modelos/articulo';
import { Categorias } from '../modelos/categorias';

@Injectable({
  providedIn: 'root'
})
export class ArticulosService {
  public oculta: number = 10;
  private articulo: Articulo[] = [{
    titulo: "2001 Odisea",
    descripcion: " Un sobrecogedor viaje interestelar en busca de la evidencia de que el ser humano no está solo en el cosmos. Una expedición a los confines del universo y a los del alma, en la que pasado, presente y futuro se amalgaman en un continuo enigmático.",
    imagen: "https://upload.wikimedia.org/wikipedia/en/7/77/2001_A_Space_Odyssey-Arthur_C._Clarke.jpg",
    id: "001",
    autor: "Arthur C. Clarke",
    fechaCreacion: "1945-12-10",
    categorias:[
      {
        id:"1",
        nombre:"Aventura",
         color:"red",
        }
      ]
  },
  {
    titulo: "1984",
    descripcion: "Se trata de una distopía. Winston Smith trabaja como censor en el Ministerio de la Verdad. Su labor consiste en una constante revisión de la historia para adecuarla a las circunstancias y alianzas del presente",
    imagen: "https://www.mrbooks.com/mrbooks/portadas/9789584296078.jpg",
    id: "002",
    autor: "George Orwell",
    fechaCreacion: "1949-11-10",
    categorias:[
      {
        id:"2",
        nombre:"Ciencia Ficcion",
         color:"blue",
        }
      ]
  },
  {
    titulo: "Nada",
    descripcion: " Un sobrecogedor viaje interestelar en busca de la evidencia de que el ser humano no está solo en el cosmos. Una expedición a los confines del universo y a los del alma, en la que pasado, presente y futuro se amalgaman en un continuo enigmático.",
    imagen: "https://images-na.ssl-images-amazon.com/images/I/91lFYpSZ2xL.jpg",
    id: "003",
    autor: "Carmen Laforet",
    fechaCreacion: "1945-05-18",
    categorias:[
      {
        id:"3",
        nombre:"Epico",
         color:"orange",
        }
      ]
  },
  {
    titulo: "La Divina Comedia",
    descripcion: " Como se sabe, la Divina Comedia cuenta el maravilloso viaje del autor por el Infierno, el Purgatorio y el Paraíso, guiado por Virgilio y con la intermediación de una bella mujer de florentina llamada, Beatriz, que se convierte así en mito de la poesía amorosa.",
    imagen: "https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1315800260l/2878210.jpg",
    id: "004",
    autor: "Dante",
    fechaCreacion: "1307-12-10",
    categorias:[
      {
        id:"4",
        nombre:"Clasico",
         color:"green",
        },
        {
          id:"4",
          nombre:"Clasico",
           color:"green",
          }
      ]
  },

  {
    titulo: "Lord of the rings",
    descripcion: "The title refers to the story's main antagonist, the Dark Lord Sauron, who in an earlier age created the One Ring to rule the other Rings of Power given to Men, Dwarves, and Elves, in his campaign to conquer all of Middle-earth. ",
    imagen: "https://upload.wikimedia.org/wikipedia/en/thumb/e/e9/First_Single_Volume_Edition_of_The_Lord_of_the_Rings.gif/220px-First_Single_Volume_Edition_of_The_Lord_of_the_Rings.gif",
    id: "005",
    autor: "J.R.R. Tolkien",
    fechaCreacion: "1954-12-10",
    categorias:[
      {
        id:"5",
        nombre:"Aventura",
         color:"purple",
        }
      ]
  },
  {
    titulo: "Cien años de Soledad",
    descripcion: " Un sobrecogedor viaje interestelar en busca de la evidencia de que el ser humano no está solo en el cosmos. Una expedición a los confines del universo y a los del alma, en la que pasado, presente y futuro se amalgaman en un continuo enigmático.",
    imagen: "https://imagessl8.casadellibro.com/a/l/t7/08/9788497592208.jpg",
    id: "006",
    autor: "Gabriel Garcia Marquez",
    fechaCreacion: "1967-12-10",
    categorias:[
      {
        id:"6",
        nombre:"Drama",
         color:"pink",
        }
      ]
  }
  ]


  ngOnInit(): void {
  }
  direccion: AngularFirestoreCollection<Articulo>
  constructor(private db: AngularFirestore) {
    this.direccion = this.db.collection<Articulo>("articulos")
    
   }
  
  sizeArreglo() {
    return this.articulo.length
  }
  getArticulos() {
    return this.articulo;
  }
  getArticuloId(id: string): Articulo {
    for (let i = 0; i < this.articulo.length; i++) {
      if (this.articulo[i].id === id) {
        return this.articulo[i];
      }
    }
    return this.nuevoArticuloVacio();
  }
  getArticuloX(a: number): Articulo[] {
    let encontrados: Articulo[] = [];
    for (let i = 0; i < a; i++) {
      //articulos[i] = this.articulo[i]
      encontrados.push(this.articulo[i])
    }
    return encontrados;

  }
  nuevoArticuloVacio(): Articulo {
    return {
      id: "",
      titulo: "",
      descripcion: "",
      autor: "",
      fechaCreacion: "",
      imagen: "",
      categorias:[]
    }
  }
  /*Doesn't work
    getXArticulos(x:number){
      let res:Articulo[]=[]
      let a:number=this.generar(0,0)
      let b:number=this.generar(0,0)
      let c:number=this.generar(0,0)
      if(a===b || a==c){
       a=this.generar(0,0)
      }
      if(c===b || c==a){
        c=this.generar(0,0)
      }
      if(b===a || b==c){
        b=this.generar(0,0)
      }
      res.push(this.articulo[a])
      res.push(this.articulo[b])
      res.push(this.articulo[c])
    return true
    }
  */
  generar(maximo: number, minimo: number) {
    return Math.floor(Math.random() * (1 + maximo - minimo) + minimo)

  }
  reordernar(n: number) {
    let original: number[] = [];
    let temporal: number = 0;
    let contador: number = 0;
    let arreglodeArticuloRandom: Articulo[] = [];

    for (let i = 0; i < n; i++) {
      original.push(i);
    }
    while (contador < n) {
      let random = Math.floor(Math.random() * n);
      console.log(random);
      temporal = original[contador];
      original[contador] = original[random];
      original[random] = temporal;
      contador++
    }
    for (let i = 0; i < original.length; i++) {
      arreglodeArticuloRandom.push(this.articulo[original[i]]);
    }
    return arreglodeArticuloRandom;
  }
  buscador(buscar: string): Articulo[] {

    if (buscar.length === 0) {
      return this.articulo;
    }
    return this.articulo.filter(arts => {
      return arts.titulo.toLowerCase().indexOf(buscar.toLowerCase()) > -1
    })
  }
}