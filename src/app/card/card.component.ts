import { Component, OnInit, Input } from '@angular/core';
import { Articulo } from '../modelos/articulo';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
@Input("helicoptero") misArticulos?: Articulo[] 
  constructor() { }

  ngOnInit(): void {
  }

}
