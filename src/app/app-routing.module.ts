import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ArticuloComponent } from './articulo/articulo.component';
import { ContactoComponent } from './contacto/contacto.component';
import { InicioComponent } from './inicio/inicio.component';
import { SingularComponent } from './singular/singular.component';
import { TodosBlogsComponent } from './todos-blogs/todos-blogs.component';
import {MatInputModule} from '@angular/material/input';

const routes: Routes = [
  {
    component:ContactoComponent,
    path: 'contacto'
  },
  
  {
    component: InicioComponent,
    path: 'inicio'
  },
    
  {
    component: ArticuloComponent,
    path: 'articulo'
  },
  {
    component: SingularComponent,
    path: 'articulos/:rueda'
  },
  {
    component: TodosBlogsComponent,
    path: 'todosblogs'
  },
  
  {
    path:"admin",
    loadChildren:() => import("./admin/admin.module").then(m=>m.AdminModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
