import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Articulo } from '../modelos/articulo';
import { ArticulosService } from '../servicios/articulos.service';

@Component({
  selector: 'app-singular',
  templateUrl: './singular.component.html',
  styleUrls: ['./singular.component.scss']
})
export class SingularComponent implements OnInit {
  //iniciar string vacio
  direccion: string = ""
  articulo!: Articulo;
  recomendado: Articulo[]=[];
  arreglodeArticulos:Articulo[]=[];
  arreglodeArticuloRandom:Articulo[]=[];
  constructor(private rutasS: ActivatedRoute, private articuloS: ArticulosService) {
    //this.direccion = this.rutasS.snapshot.params.rueda
    this.rutasS.params.subscribe(x => {
      this.direccion = x.rueda;
      this.articulo = this.articuloS.getArticuloId(this.direccion);
    });
   //this.recomendado = this.articuloS.getArticuloX(3);
    this.recomendado = this.articuloS.reordernar(6);
  }
  ngOnInit(): void {
  }

}
