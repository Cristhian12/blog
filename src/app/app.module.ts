
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { CardComponent } from './card/card.component';
import { ContactoComponent } from './contacto/contacto.component';
import { InicioComponent } from './inicio/inicio.component';
import { ArticuloComponent } from './articulo/articulo.component';
import { SingularComponent } from './singular/singular.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { CompartidoModule } from './compartido/compartido.module';
import { TodosBlogsComponent } from './todos-blogs/todos-blogs.component';
import { MatInputModule } from '@angular/material/input';
import { AdminModule } from './admin/admin.module';
import {AngularFireModule} from "@angular/fire/compat"
import {environment} from "../environments/environment";
import {AngularFirestoreModule} from "@angular/fire/compat/firestore"

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    CardComponent,
    ContactoComponent,
    InicioComponent,
    ArticuloComponent,
    SingularComponent,
    TodosBlogsComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,

    MatButtonModule,
    NoopAnimationsModule,
    FontAwesomeModule,
    CompartidoModule,
    MatInputModule,
    AdminModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
