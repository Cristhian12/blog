import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TodosBlogsComponent } from './todos-blogs.component';

describe('TodosBlogsComponent', () => {
  let component: TodosBlogsComponent;
  let fixture: ComponentFixture<TodosBlogsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TodosBlogsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TodosBlogsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
