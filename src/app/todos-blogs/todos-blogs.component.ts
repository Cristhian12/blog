import { Component, OnInit } from '@angular/core';
import { Articulo } from '../modelos/articulo';
import { ArticulosService } from '../servicios/articulos.service';

@Component({
  selector: 'app-todos-blogs',
  templateUrl: './todos-blogs.component.html',
  styleUrls: ['./todos-blogs.component.scss']
})
export class TodosBlogsComponent implements OnInit {
  arreglodeArticulos: Articulo[] = [];
buscar: string="";
  constructor(private articuloS:ArticulosService) {

    
  }

  ngOnInit(): void {
    
  }


  filtrador(buscar:string){
    return this.articuloS.buscador(buscar)
  }

}
