import { Categorias } from "./categorias";

export interface Articulo {
    titulo: string,
    descripcion: string,
    imagen: string,
    id?: string,
    autor: string,
    fechaCreacion: string,
    categorias:Categorias[],

}
