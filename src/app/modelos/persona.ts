export interface Persona {
    nombre:String;
    casado:boolean,
    mascotas:String[],
    edad:number,
}
