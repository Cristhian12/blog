export interface Contacto {
    id: string;
    nombre: string;
    correo: string;
    mensaje: string;
    telefono: string;
}
