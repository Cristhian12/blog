import { Component, OnInit } from '@angular/core';
import { Articulo } from '../modelos/articulo';
import { Persona } from '../modelos/persona';
import { ArticulosService } from '../servicios/articulos.service';
import { CategoriasService } from '../servicios/categorias.service';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.scss']
})
export class InicioComponent implements OnInit {

  articulo:Articulo[]=[]
  card:Articulo[]=[]
  title = 'Blog Cristhian';
  jirafa = 20;
  cajas: number = 50 + 20;
  nombre: string = "Cristhian";
  mascota: string = `Willson`;
  casado: boolean = true;
  colegio: number[] = [30, 31, 32, 33];
  mascotas: string[] = ["Gorge", "Paula", ""];
  esUnPerro: boolean[] = [true, false, true]
  personaUno: Persona = {
    nombre: "Cristhian Barros", 
    casado: true,
    mascotas: ["Firulais", "boby"],
    edad: 18
  }
  //arreglo []
  valor: number = 0
  clase: Persona[] = [
    {
      nombre: "Juanita Lopez",
      casado: false,
      mascotas: ["Fido"],
      edad: 16
    },
    {
      nombre: "Juan Lopez",
      casado: false,
      mascotas: ["Max, Mex"],
      edad: 16
    },
    {
      nombre: "Jordan Chango",
      casado: false,
      mascotas: ["Pelusa"],
      edad: 17
    }
  ]
  

  constructor(private articuloS: ArticulosService, private categoriaS: CategoriasService) {

    console.log(this.articuloS.getArticulos()[0].descripcion)
    console.log(this.articuloS.getArticuloId("003"))
    console.log(this.articuloS.getArticuloX(1))
    this.card=this.articuloS.getArticulos()
    this.categoriaS.buscarCategoria("FmlGTDDXvWpa6mQc1ohU").subscribe(val=>{
      console.log(val)
    })
  }
  ngOnInit(): void {
    console.log("Hola estoy en el Oninit")
    this.sumador(10, 15)
    this.verPrimo(19)
  }
  ngOnDestroy(): void {
    console.log("Estoy en el junk destroyer")
    this.sumador(10, 15)
  }
  sumador(a: number, b: number): number {
    return a + b
  }
  sumaPares(a: number, b: number): number {
    if (a % 2 === 0 && b % 2 === 0) {
      return a + b
    }
    else {
      return 0
    }
  }
  sumarArreglo(a: number[]) {
    console.log()
    for (let conteo = 0; conteo < 10; conteo++) {
      console.log(conteo)
    }
  }
  /* numeroPrimo(a: number):boolean{
     let primo!: boolean;
     for(let contador=2; contador<=a; contador++){
       console.log(contador)
       if(a%contador===0){
         this.valor++;
         if(this.valor===2){
           primo = true;
           console.log(primo)
           break;
           }else{
             primo=false;
         }
       }
     }
     console.log(primo)
     return primo;
   
   }
   */
  verificarNumeroPrimo(a: number): boolean {

    let c: number = 0;
    let primo: boolean = false;

    for (let b = 1; b <= a; b++) {
      if (a % b === 0) {
        c++;
        if (c === 2) {
          primo = true
          //return pa detener 
        } else {
          primo = false
        }

      }
    }
    return primo;
  }
  verPrimo(a: number) {
    let contador: number = 0
    for (let i = 0; i <= a; i++) {
      if (a % i === 0) {
        contador++
      }

    }
    if (contador === 1) {
      return true;
    } else {
      return false;
    }
  }
}
