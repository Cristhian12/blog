// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyAA4ahIavBAlakATrdrNGOck3d1lsk8R00",
    authDomain: "blog-fd713.firebaseapp.com",
    projectId: "blog-fd713",
    storageBucket: "blog-fd713.appspot.com",
    messagingSenderId: "831667031242",
    appId: "1:831667031242:web:4bf6aa4a3934f58ab5b833"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
